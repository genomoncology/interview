### Interested in writing code with us? ###

* Fork this repository to your own private bitbucket repo
* Make all the tests pass! 
* Give us access to your private repo so we can check out your work.

### How do I get set up? ###

* This project uses Gradle. You don't need anything installed on your system except Java.  After you pull down the project run ```./gradlew test```

### What we'll be looking at / looking for: ###

* Tests
* How well you [grok](https://en.wikipedia.org/wiki/Grok) OOP
* Your workflow (as best we can infer from your commit history)

### Who do I talk to? ###

* If you get stuck or have any questions, feel free to reach out to tech@genomoncology.com