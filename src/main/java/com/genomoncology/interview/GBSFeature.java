package com.genomoncology.interview;

/**
 * Container of GBS information parsed from GBS files.
 */
public class GBSFeature {

    public enum Types { source, assembly_gap, gene, mRNA, CDS };

    private Types fType;

    public Types getType() {
        return fType;
    }

    public void setType(Types theType) {
        fType = theType;
    }

    public String getValue(String theField) {
        throw new RuntimeException("this needs to be implemented.");
    }

}
