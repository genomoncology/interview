package com.genomoncology.interview;

import java.io.*;
import java.util.Iterator;

/**
 * Takes a GBS file and parses out interesting information for processing.
 */
public class GBSParser implements Iterable<GBSFeature>, Iterator<GBSFeature> {

    private BufferedReader fReader;

    public GBSParser(InputStream theInputStream) {
        fReader = new BufferedReader(new InputStreamReader(theInputStream));
    }

    public GBSParser(String theFileLocation) throws IOException {
        this(new FileInputStream(theFileLocation));
    }

    @Override
    public Iterator<GBSFeature> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public GBSFeature next() {
        return null;
    }

    @Override
    public void remove() {
    }

    // some helper methods

    protected boolean hasContent() {
        return (fReader != null);
    }

    protected String nextLine() {
        String aLine;

        try {
            aLine = fReader.readLine();

            if (aLine == null) {
                fReader.close();
                fReader = null;
            }

        } catch (IOException e) {
            throw new RuntimeException("Invalid Reader.");
        }

        return aLine;
    }
}
