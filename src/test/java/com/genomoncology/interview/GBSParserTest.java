package com.genomoncology.interview;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Class for running tests against GBSParser code.
 *
 * You must copy the sample.gbs file to the tmp folder, hard code a path, or come with something better.
 *
 * Only the first three test should pass. Uncomment the others and make them pass.
 *
 * Feel free to add additional tests and functionality if you want to impress us.
 *
 * There is probably some bugs in the code too. Feel free to fix those as well.
 */
public class GBSParserTest {

    private static final String sFilePath = "sample.gbs";
    private GBSParser fGBSParser;

    @Before
    public void setup() throws IOException {
        fGBSParser = new GBSParser(sFilePath);
    }

    @Test
    public void shouldHaveContentFlagTrueOnConstruction() {
        Assert.assertTrue(fGBSParser.hasContent());
    }

    @Test
    public void shouldReadStrings() {
        Assert.assertEquals("LOCUS       NC_000007          159138663 bp    DNA     linear   CON 13-AUG-2013",
                            fGBSParser.nextLine());
    }

    @Test
    public void shouldHave125LinesInSample() {
        int aCounter = 0;
        String aLine;
        while (fGBSParser.hasContent()) {
            aLine = fGBSParser.nextLine();
            if (aLine != null) {
                aCounter++;
            }
        }

        Assert.assertEquals(125, aCounter);
    }

    /*********************************************************/
    /**           UNCOMMENT BELOW AND MAKE WORK....         **/
    /*********************************************************/


//    @Test
//    public void shouldHaveFeatures() {
//        Map<GBSFeature.Types, Integer> aTypesMap = new HashMap<GBSFeature.Types, Integer>();
//        for (GBSFeature aGBSFeature : fGBSParser) {
//            Assert.assertNotNull(aGBSFeature);
//            if (!aTypesMap.containsKey(aGBSFeature.getType())) {
//                aTypesMap.put(aGBSFeature.getType(), 0);
//            }
//            aTypesMap.put(aGBSFeature.getType(), aTypesMap.get(aGBSFeature.getType()) + 1);
//        }
//
//        Assert.assertEquals(1, aTypesMap.get(GBSFeature.Types.source).intValue());
//        Assert.assertEquals(1, aTypesMap.get(GBSFeature.Types.assembly_gap).intValue());
//        Assert.assertEquals(1, aTypesMap.get(GBSFeature.Types.mRNA).intValue());
//        Assert.assertEquals(2, aTypesMap.get(GBSFeature.Types.gene).intValue());
//        Assert.assertEquals(2, aTypesMap.get(GBSFeature.Types.CDS).intValue());
//    }
//
//    @Test
//    public void shouldHaveTwoGenes() {
//        List<String> aGeneList = new ArrayList<String>();
//        for (GBSFeature aGBSFeature : fGBSParser) {
//            if (aGBSFeature.getType() == GBSFeature.Types.gene) {
//                aGeneList.add(aGBSFeature.getValue("gene"));
//            }
//        }
//
//        Assert.assertTrue("Missing: BRAF", aGeneList.contains("BRAF"));
//        Assert.assertTrue("Missing: AGK", aGeneList.contains("AGK"));
//    }


}
